console.log("hello jake");
/*
	selection control structures 
		sorts out whether tje statements are to be executed based on the condition whether it is true or false

		if else
		switch
		try catch finally 

		if else statement 
			if(condition){
				statement
			}
			else{
				statement
			}
*/

// if statement 
// execute a statement if a sepcified condition is true
//can stand alon even without the else statement

let numA =-1;

if (numA < 0){
	console.log("helloooo");
}
//if false no output

// else if 
let numB = 1;
if (numB < 0){
	console.log("hello");
}
else if (numB > 0){
	console.log("world");
}

//

/*
	mini activity 
		create conditional statement thati f the height is below 150, display "did not pass minimum height requirment"

		if above or equal 150, "passed the minimum height req"

		put it inside the function
*/



function getHeight(height){
	if (height <= 150){
		console.log("did not pass minimum height requirment");
	}
	else{
			console.log(" pass minimum height requirment");
		
	}
}

getHeight(154);


// if else elseif  statement w/ function

let message = "no message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30 ){
		return "not typhon"
	}
	else if(windSpeed <=61){
		return "tropical depression"
	}
	else if (windSpeed >= 62 && windSpeed <=88){
		return "tropical storm"
	}
	else if (windSpeed >=89 && windSpeed <=117){
		return "severe tropical storm"
	}
	else{
		return "typhoon"
	}
}


message = determineTyphoonIntensity(70);
console.log(message);

// message = determineTyphoonIntensity(200);
// console.log(message);

// message = determineTyphoonIntensity(10);
// console.log(message);

// message = determineTyphoonIntensity(40);
// console.log(message);

if (message === "tropical storm"){
	console.warn(message);
	console.error(message);
}




// truthy 
if (true ){
	console.log("truthy");
}


if (1){
	console.log("truthy");
}


if ([]){
	console.log("truthy");
}

// conditional (ternary) operator
/*
	syntax
		(expression) ? ifTrue : ifFalse;
*/

// single statement execution
// ternary operators have an implicit "return" statement, meaning that without the returning keyword, the resulting expression can be stored in a variable
let ternaryResult = (1 < 18) ? "istrue" : "isfalse";
console.log("result of ternary operator "  + ternaryResult);


// multiple statement execution
/*let name = prompt("enter your name");
function isOfLegalAge(){
	// name = "john";
	return "you are legal age";
};
function isOfUnderAge(){
	// name = "jane";
	return "underage";
};
// parseInt() convert the input received into a number data type
let age1 = parseInt(prompt ("what is ur age?"));
console.log(age1);
console.log(typeof age1)
let legalAge = (age1 > 18) ?isOfLegalAge() : isOfUnderAge();
console.log("result: " + legalAge +" "+name);
	*/
// switch statement 


let day = prompt("what day of the week today") .toLowerCase();
console.log(day);

switch(day){
case 'mon':
	console.log("ayaw mo na dumating na day");
break

case 'tue' :
	console.log("nakakapagod");
break

case 'wed' :
	console.log("nakalahati mo na ung pasok");
break

case 'thu' :
	console.log("tiis nalng");
break

case 'fri' :
	console.log("ohh yeah");
break

case 'sat' :
	console.log("finally rest");
break

case 'sun' :
	console.log("gala");
break

default:
	console.log("patay ka na di mo alam kung anong araw");
}

// try catch fianally statement
// try catch is for error handling
function shoeIntensityAlert (windSpeed){
	try{
		// attempt to execute a code 
		alerat(determineTyphoonIntensity(windSpeed))
	}

	catch(error){
		console.log(error)
		console.log(error.message)
	}

	finally{
		//will continue execution of code regardless of success and failure of code execution in the "try " block to handle/resolve errors
		alert("intesity updates will show new alert");
	}


}
//shoeIntensityAlert(56)













